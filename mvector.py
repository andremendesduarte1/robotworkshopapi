import anki_vector
import sys
from anki_vector.util import degrees, distance_mm, speed_mmps
import time
from anki_vector.events import Events

# HELPER ######################################
robot = None
helperVariable = None

def __try_initial_setup():
    global robot
    if robot == None:
        sys.argv = [sys.argv[0]]
        args = anki_vector.util.parse_command_args()
        robot = anki_vector.Robot(enable_face_detection=True)
        robot.connect()

def desligar():
    if robot != None:
        robot.disconnect()

# VOZ #########################################

def dizer_frase(frase):
    __try_initial_setup()
    robot.behavior.say_text(frase)


# MOVIMENTO #################################################

def andar_em_frente(mm, mmps):
    __try_initial_setup()
    robot.behavior.drive_straight(distance_mm(mm), speed_mmps(mmps))


def rodar(graus):
    __try_initial_setup()
    robot.behavior.turn_in_place(degrees(graus))

def pegar_no_cubo():
    __try_initial_setup()
    robot.world.connect_cube()

    if robot.world.connected_light_cube:
        dock_response = robot.behavior.dock_with_cube(
            robot.world.connected_light_cube,
            num_retries=3)
        if dock_response:
            docking_result = dock_response.result

        robot.world.disconnect_cube()


def rodar_cabeca(graus):
    __try_initial_setup()
    robot.behavior.set_head_angle(degrees(graus))

def posicionar_braco(altura_0_a_1):
    __try_initial_setup()
    robot.behavior.set_lift_height(altura_0_a_1)

def cubo_visivel():
    __try_initial_setup()
    print(robot.world.visible_objects)
    for obj in robot.world.visible_objects:
        print(obj)
        if isinstance(obj, anki_vector.objects.LightCube):
            return True
    return False

def test_subscriber(robot, event_type, event):
    global helperVariable
    for face in robot.world.visible_faces:
        if face.name == "":
            helperVariable = ""
        else:
            helperVariable = face.name
        return


def detectar_pessoa():
    __try_initial_setup()
    global helperVariable
    helperVariable = None
    # If necessary, move Vector's Head and Lift to make it easy to see his face
    robot.events.subscribe(test_subscriber, Events.robot_changed_observed_face_id)
    robot.events.subscribe(test_subscriber, Events.robot_observed_face)

    while helperVariable == None:
        pass
    return helperVariable