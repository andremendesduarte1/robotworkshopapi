import cozmo
from cozmo.util import degrees, distance_mm, speed_mmps
import time
from cozmo.lights import Light, Color
from cozmo import lights
from cozmo.objects import LightCube
import asyncio

helperVariable = None
helperVariable1 = None
helperVariable2 = None
helperVariable3 = None

map_color_to_light = {
    'preto' : Light(Color(name='preto', rgb = (0, 0, 0)), Color(name='preto', rgb = (0, 0, 0))),
    'branco' : Light(Color(name='branco', rgb = (255, 255, 255)), Color(name='branco', rgb = (255, 255, 255))),
    'cinzento' : Light(Color(name='cinzento', rgb = (128, 128, 128))),
    'verde' : lights.green_light, 
    'verde escuro' : Light(Color(name='verde escuro', rgb = (0, 128, 0))),
    'amarelo' : Light(Color(name='amarelo', rgb = (255, 255, 0))), 
    'azul' : lights.blue_light,
    'azul claro' : Light(Color(name='azul claro', rgb = (0, 255, 255))),
    'vermelho' : lights.red_light,
    'vermelho escuro' : Light(Color(name='vermelho escuro', rgb = (128, 0, 0))),
    'roxo' : Light(Color(name='roxo', rgb = (128, 0, 128))),
    'roxo claro' : Light(Color(name='roxo claro', rgb = (255, 0, 255))),
    'violeta' : Light(Color(name = 'violeta', rgb = (238,130,238))),
    'laranja' : Light(Color(name='laranja', rgb = (255, 69, 0))),
    'laranja claro' : Light(Color(name='laranja claro', rgb = (255,165,0))),
    'rosa' : Light(Color(name='rosa', rgb = (255,20,147))),
    'castanho' : Light(Color(name='castanho claro', rgb = (210,105,30)))}
    #'castanho' : Light(Color(name='castanho claro', rgb = (255,222,173))),
    #'castanho escuro' : Light(Color(name='castanho', rgb = (255,235,205)))}

# VOZ #########################################

def dizer_frase(frase):
    global helperVariable
    helperVariable = frase
    cozmo.run_program(__dizer_frase)

async def __dizer_frase(robot):
    await robot.say_text(helperVariable).wait_for_completed()

# MOVIMENTO #################################################

def andar_em_frente(mm, mmps):
    global helperVariable
    global helperVariable1
    helperVariable = mm
    helperVariable1 = mmps
    cozmo.run_program(__andar_em_frente)


async def __andar_em_frente(robot):
    await robot.drive_straight(distance_mm(helperVariable), speed_mmps(helperVariable1)).wait_for_completed()

def rodar(graus):
    global helperVariable
    helperVariable = graus
    cozmo.run_program(__rodar)

async def __rodar(robot):
    await robot.turn_in_place(degrees(helperVariable)).wait_for_completed()

def pegar_no_cubo():
    cozmo.run_program(__pegar_no_cubo)

async def __pegar_no_cubo(robot):
    cube = await robot.world.wait_for_observed_light_cube()

    await robot.dock_with_cube(cube, approach_angle=cozmo.util.degrees(0), num_retries=2).wait_for_completed()

def rodar_cabeca(graus):
    global helperVariable
    helperVariable = graus
    cozmo.run_program(__rodar_cabeca)

async def __rodar_cabeca(robot):
    await robot.set_head_angle(degrees(helperVariable)).wait_for_completed()

def posicionar_braco(altura_0_a_1):
    global helperVariable
    helperVariable = altura_0_a_1
    cozmo.run_program(__posicionar_braco)
 
async def __posicionar_braco(robot):
    await robot.set_lift_height(helperVariable).wait_for_completed()


def cubo_visivel():
    cozmo.run_program(__cubo_visivel)
    return helperVariable == "Cubo"

async def __cubo_visivel(robot):
    global helperVariable
    helperVariable = ""
    cube = None

    try:
        cube = await robot.world.wait_for_observed_light_cube(timeout=1)
        helperVariable = "Cubo"
    except asyncio.TimeoutError:
        print("Didn't find a cube")

def detectar_pessoa():
    cozmo.run_program(__detectar_pessoa)
    return helperVariable

async def __detectar_pessoa(robot):
    global helperVariable
    helperVariable = ""
    face = None
    while True:
        if face and face.is_visible:
            helperVariable = face.name
            break
        else:
            try:
                face = await robot.world.wait_for_observed_face(timeout=30)
            except asyncio.TimeoutError:
                print("Didn't find a face.")


def cores_das_luzes(cor1, cor2, cor3, duration):
    global helperVariable
    global helperVariable1
    global helperVariable2
    global helperVariable3
    helperVariable = cor1
    helperVariable1 = cor2
    helperVariable2 = cor3
    helperVariable3 = duration
    cozmo.run_program(__cores_das_luzes)

def __cores_das_luzes(robot):
    print(helperVariable)
    print(helperVariable1)
    print(helperVariable2)
    print(lights.off_light)
    robot.set_backpack_lights(lights.off_light,
                              map_color_to_light[helperVariable],
                              map_color_to_light[helperVariable1],
                              map_color_to_light[helperVariable2],
                              lights.off_light)
    print("DURACAO : " + str(helperVariable3))
    time.sleep(helperVariable3)

def cor_da_luz(cor1, duration):
    global helperVariable
    global helperVariable1
    global helperVariable2
    global helperVariable3
    helperVariable = cor1
    helperVariable1 = cor1
    helperVariable2 = cor1
    helperVariable3 = duration
    cozmo.run_program(__cores_das_luzes)
